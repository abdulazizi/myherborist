import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/Storage';

import { DashboardPage } from '../dashboard/dashboard';
import { LoginPage } from '../login/login';

// import * as CryptoJS from 'crypto-js';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  loader: any;

  fullname: any;
  phone: any;
  password: any;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  createIdMember() {
    // this.navCtrl.push(DashboardPage)

    // var newDate = new Date();
    // var dateTime: String = new Date(newDate.getTime() - newDate.getTimezoneOffset() * 60000).toISOString();

    var dateTime2 = Math.floor(Date.now() / 10).toString();
    console.log('Math.floor(Date.now() / 10).toString() : ', dateTime2);

    // console.log(this.phone)
    // console.log(dateTime)

    var last4digitphone = this.phone.substring(this.phone.length - 4);
    // var year = dateTime.substring(2, 4);
    // var month = dateTime.substring(5, 7);
    // var date = dateTime.substring(8, 10);
    // var time = dateTime.substring(11, 13);
    // var minute = dateTime.substring(14, 16);
    // var second = dateTime.substring(17, 19);

    // const convert = {
    //   bin2dec: s => parseInt(s, 2).toString(10),
    //   bin2hex: s => parseInt(s, 2).toString(16),
    //   dec2bin: s => parseInt(s, 10).toString(2),
    //   dec2hex: s => parseInt(s, 10).toString(16),
    //   hex2bin: s => parseInt(s, 16).toString(2),
    //   hex2dec: s => parseInt(s, 16).toString(10)
    // };

    // console.log('bin2dec : ', convert.bin2dec(year))
    // console.log('dec2hex : ', convert.dec2hex(year))
    // console.log('hex2bin : ', convert.hex2bin(year))
    // console.log('dec2bin : ', convert.dec2bin(year))

    var id_member = dateTime2 + last4digitphone;

    // var id_member_origin = year + month + last4digitphone + date + time + minute + second;
    // var id_member_custom = year + month + '-' + last4digitphone + '-' + date + time + '-' + minute + second;
    // var id_member_converted =
    //   parseInt(month, 16).toString() +
    //   parseInt(year, 16).toString() +
    //   '-' +
    //   parseInt(last4digitphone, 12).toString()
    //   + '-' +
    //   parseInt(date, 16).toString() +
    //   parseInt(time, 16).toString()
    //   + '-' +
    //   parseInt(minute, 16).toString() +
    //   parseInt(second, 16).toString();

    // this.key = '';
    // var key_enc = CryptoJS.AES.encrypt(this.key, id_member_origin).toString();
    // var key_dec = CryptoJS.AES.decrypt(this.key, id_member_origin).toString(CryptoJS.enc.Utf8);

    // console.log('year', year)
    // console.log('month', month)
    // console.log('date', date)
    // console.log('time', time)
    // console.log('minute', minute)
    // console.log('second', second)
    // console.log('last4digitphone', last4digitphone)

    // console.log('year', convert.dec2hex(year))
    // console.log('month', convert.dec2hex(month))
    // console.log('date', convert.dec2hex(date))
    // console.log('time', convert.dec2hex(time))
    // console.log('minute', convert.dec2hex(minute))
    // console.log('second', convert.dec2hex(second))
    // console.log('last4digitphone', convert.dec2hex(last4digitphone))

    // console.log('id member origin : ', id_member_origin)
    // console.log('id member : ', id_member_custom)
    // console.log('id member converted: ', id_member_converted)
    console.log('id member: ', id_member)

    const alert = this.alertCtrl.create({
      mode: 'ios',
      title: 'Halo ' + this.fullname,
      subTitle: 'Member ID kamu adalah' + '<br>' + id_member,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(DashboardPage)
          }
        }
      ]
    });
    alert.present();

  }

  checkNumber() {

    let body = {
      fullname: this.fullname,
      phone: this.phone,
      aksi: 'signup'
    };

    this.presentLoading('Mohon tunggu..');

    this.postPvdr.postData(body, 'Signup').subscribe((data) => {
      // var msg = data.msg;
      if (data.success) {
        this.loader.dismiss();
        // code here
        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Halo ' + this.fullname,
          subTitle: 'Nomor handphone kamu sudah terdaftar.',
          buttons: [
            {
              text: 'Tutup',
              handler: () => {
                //code here
              }
            },
            {
              text: 'Login',
              handler: () => {
                //code here
                this.navCtrl.setRoot(LoginPage)
              }
            }
          ]
        });
        alert.present();

      } else {

        this.loader.dismiss();
        // code here
        var newDate = new Date();
        var dateTime: String = new Date(newDate.getTime() - newDate.getTimezoneOffset() * 60000).toISOString();
        var dateAndTime = dateTime.substring(0, 10) + ' ' + dateTime.substring(11, 19)

        var dateTime2 = Math.floor(Date.now() / 10).toString();
        var last4digitphone = this.phone.substring(this.phone.length - 4);
        var id_member = dateTime2 + last4digitphone;
        // console.log('id member: ', id_member)

        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Halo ' + this.fullname,
          subTitle: 'Member ID kamu adalah' + '<br>' + id_member,
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                // this.SignUpProccess(this.fullname, this.phone, id_member, dateAndTime);
                var firtsphone = this.phone.substring(0, 2)
                var slicephone = this.phone.slice(2)
                var lengthphone = this.phone.length
                console.log(firtsphone)
                console.log(slicephone)
                console.log(lengthphone)
                if (lengthphone > 9) {
                  if (firtsphone == '62') {
                    var phone = '0' + slicephone
                    this.SignUpProccess(this.fullname, phone, this.password, id_member, dateAndTime);
                  } else {
                    this.SignUpProccess(this.fullname, this.phone, this.password, id_member, dateAndTime);
                  }
                } else {
                  const alert = this.alertCtrl.create
                    ({
                      mode: 'ios',
                      title: 'Pehatian',
                      subTitle: 'Kayanya nomor hanphone nya kurang deh.',
                      buttons: ['OK']
                    });
                  alert.present();
                }
              }
            }
          ]
        });
        alert.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();
    });

  }

  login() {
    this.navCtrl.setRoot(LoginPage);
  }

  SignUpProccess(name, phone, password, id_member, dateAndTime) {

    let body = {
      fullname: name,
      phone: phone,
      password: password,
      id_member: id_member,
      datenow: dateAndTime,
      aksi: 'signup_proccess'
    };

    console.log(body);

    this.presentLoading('Mohon tunggu..');

    this.postPvdr.postData(body, 'Signup').subscribe((data) => {
      // var msg = data.msg;
      if (data.success) {
        this.loader.dismiss();
        // code here
        // this.storage.set('session_user_member', data.result);
        // this.navCtrl.setRoot(DashboardPage);
        const alert = this.alertCtrl.create
          ({
            mode: 'ios',
            title: 'Berhasil',
            subTitle: 'Yeay, kamu berhasil menjadi member Herborist, sekarang silahkan login menggunakan nomor handphone terdaftar.',
            buttons: [
              {
                text: 'Oke',
                handler: () => {
                  // code here
                  this.navCtrl.setRoot(LoginPage);
                }
              }
            ]
          });
        alert.present();

      } else {

        this.loader.dismiss();
        // code here


      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();

    });

  }

}
