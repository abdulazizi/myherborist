import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/Storage';

import { DashboardPage } from '../dashboard/dashboard';
import { SignupPage } from '../signup/signup';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loader: any;
  phone: any;
  password: any;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  login() {

    console.log(this.phone)

    if (this.phone == '' || this.phone == null) {
      const alert = this.alertCtrl.create({
        mode: 'ios',
        title: 'Perhatian',
        subTitle: 'Kamu belum isi nomor handphone.',
        buttons: [
          {
            text: 'Tutup',
            handler: () => {
              // code here
            }
          }
        ]
      });
      alert.present();

    } else {

      this.loginProccess();

    }
  }

  loginProccess() {
    let body = {
      phone: this.phone,
      password: this.password,
      aksi: 'login'
    };

    this.presentLoading('Mohon tunggu..');

    this.storage.clear();

    this.postPvdr.postData(body, 'Login').subscribe((data) => {
      console.log(data.result)
      // var msg = data.msg;
      if (data.success) {
        this.loader.dismiss();
        // code here
        this.storage.set('session_user_member', data.result);
        this.navCtrl.setRoot(DashboardPage)

      } else {
        this.loader.dismiss();
        // code here
        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Info',
          subTitle: 'Kamu belum terdaftar sebagai member Herborist, yuk daftar dulu.',
          buttons: [
            {
              text: 'Nanti Aja',
              handler: () => {
                // code here
              }
            },
            {
              text: 'Daftar',
              handler: () => {
                // code here
                this.navCtrl.setRoot(SignupPage)
              }
            }
          ]
        });
        alert.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();
    });
  }

  signup() {
    this.navCtrl.setRoot(SignupPage)
  }

}
