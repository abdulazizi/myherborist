import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, Platform, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/Storage';

import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';
import { SignupPage } from '../signup/signup';
import { TransactionPage } from '../transaction/transaction';
import { PostProvider } from '../../providers/post-provider';
import { DigitalcardPage } from '../digitalcard/digitalcard';
import { TransdetailPage } from '../transdetail/transdetail';


@IonicPage()

@Component({
  templateUrl: 'menuspage.html'
})
export class MenusPage {

  constructor(
    public viewCtrl: ViewController,
    public storage: Storage
  ) {


  }

  ionViewDidLoad() {
    // console.log('menusPage')
    this.storage.get('session_user_member').then((res) => {
      if (res == null) {
        document.getElementById('signup_button').style.display = '';
        document.getElementById('profile_button').style.display = 'none';
        document.getElementById('logout_button').style.display = 'none';
      } else {
        document.getElementById('signup_button').style.display = 'none';
        document.getElementById('profile_button').style.display = '';
        document.getElementById('logout_button').style.display = '';
      }
    });
  }

  signUp() {
    this.storage.clear();
    this.viewCtrl.dismiss({ menu: 'signup' });
  }

  profilePage() {
    this.viewCtrl.dismiss({ menu: 'profile' });
  }

  transPage() {
    this.viewCtrl.dismiss({ menu: 'transaction' });
  }

  logout() {
    this.storage.clear();
    this.viewCtrl.dismiss({ menu: 'logout' });
  }


}

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  browser: any;
  phoneNumber: any;

  point: any;
  fullname: any;
  id_member: any;
  phone: any;

  loader: any;

  trans_list: any;

  constructor(
    public popoverCtrl: PopoverController,
    public plt: Platform,
    public storage: Storage,

    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,

    public navCtrl: NavController,
    public navParams: NavParams) {

    // console.log(this.plt.platforms());
    // console.log(this.plt.width());

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }


  ionViewDidEnter() {
    if (this.plt.is('core')) {
      this.browser = 1;
      // console.log('Browser')
    } else {
      this.browser = 0;
    }

    this.getTrans();

    this.storage.get('session_user_member').then((res) => {
      // console.log(res)
      this.id_member = res[0].id_member;
      this.phone = res[0].phone;
      this.fullname = res[0].full_name.toUpperCase();
      this.getPoint(this.id_member, this.phone)

    });

  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  getPoint(id_member, phone) {
    let body = {
      phone: phone,
      id_member: id_member,
      aksi: 'get_point'
    };

    // console.log(body);

    // this.presentLoading('Mohon tunggu..');

    this.postPvdr.postData(body, 'Transaction').subscribe((data) => {

      if (data.success) {
        // this.loader.dismiss();
        // code here

        this.point = data.result[0].balance;


      } else {

        // this.loader.dismiss();
        // code here

        this.point = '0';

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();

    });
  }

  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getPoint(this.id_member, this.phone)

    setTimeout(() => {
      // console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  searchNumberPhone() {
    console.log(this.phoneNumber.substring(0, 2))
  }

  presentPopover(ev) {
    let popover = this.popoverCtrl.create(MenusPage);
    popover.present({
      ev: ev
    });
    popover.onDidDismiss(data => {

      if (data) {
        if (data.menu == 'profile') {
          this.navCtrl.push(ProfilePage)
        }
        else if (data.menu == 'signup') {
          this.navCtrl.setRoot(SignupPage)
        }
        else if (data.menu == 'logout') {
          this.navCtrl.setRoot(LoginPage)
        }
        else if (data.menu == 'transaction') {
          this.navCtrl.push(TransactionPage)
        }
      }

    });
  }

  goDigitalCard() {
    this.navCtrl.push(DigitalcardPage)
  }

  getTrans() {
    this.storage.get('session_user_member').then((res) => {

      this.id_member = res[0].id_member;
      this.phone = res[0].phone;

      let body = {
        phone: this.phone,
        id_member: this.id_member,
        aksi: 'get_trans'
      };

      // console.log(body);

      this.presentLoading('Mohon tunggu..');

      this.postPvdr.postData(body, 'Transaction').subscribe((data) => {

        if (data.success) {
          this.loader.dismiss();
          // code here

          this.trans_list = [];
          var tgl = "";
          var tgl_1 = "";
          var tgl_2 = "";
          for (let i = 0; i < data.result.length; i++) {
            tgl_1 = data.result[i].just_date;
            if (tgl_1 != tgl_2) {
              tgl = tgl_1;
            } else {
              tgl = "";
            }
            this.trans_list.push({
              'add_date': data.result[i].add_date,
              'color': data.result[i].color,
              'counter_id': data.result[i].counter_id,
              'counter_name': data.result[i].counter_name,
              'id': data.result[i].id,
              'id_member': data.result[i].id_card,
              'just_date': tgl,
              'date': data.result[i].just_date,
              'name_location': data.result[i].name_location,
              'phone': data.result[i].phone,
              'point': data.result[i].point,
              'ref_number': data.result[i].ref_number,
              'tipe': data.result[i].tipe,
              'type': data.result[i].type
            })
            tgl_2 = data.result[i].just_date;
          }

          // console.log(this.trans_list)

        } else {

          this.loader.dismiss();
          // code here

          document.getElementById('empty').style.display = '';

        }
      }, error => {

        this.loader.dismiss();
        const alert = this.alertCtrl.create
          ({
            mode: 'ios',
            title: 'Pehatian',
            subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
            buttons: ['OK']
          });
        alert.present();

      });

    });
  }
  getDetailTrans(
    add_date,
    color,
    counter_id,
    counter_name,
    id,
    id_member,
    date,
    name_location,
    phone,
    point,
    ref_number,
    tipe,
    type) {

    // const modal = this.modalCtrl.create(TransdetailPage, {
    //   add_date: add_date,
    //   color: color,
    //   counter_id: counter_id,
    //   counter_name: counter_name,
    //   id: id,
    //   id_member: id_member,
    //   date: date,
    //   name_location: name_location,
    //   phone: phone,
    //   point: point,
    //   ref_number: ref_number,
    //   tipe: tipe,
    //   type: type
    // });
    // modal.present();

    this.navCtrl.push(TransdetailPage, {
      add_date: add_date,
      color: color,
      counter_id: counter_id,
      counter_name: counter_name,
      id: id,
      id_member: id_member,
      date: date,
      name_location: name_location,
      phone: phone,
      point: point,
      ref_number: ref_number,
      tipe: tipe,
      type: type
    })
  }

}
