import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/Storage';

import QRCode from 'qrcode';

// import * as html2canvas from 'html2canvas';
import html2canvas from 'html2canvas';


@IonicPage()
@Component({
  selector: 'page-digitalcard',
  templateUrl: 'digitalcard.html',
})
export class DigitalcardPage {

  qrcodeId: any;

  id_member: any;
  fullname: any;
  phone: any;
  birthday: any;
  email: any;
  nik: any;

  constructor(
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.storage.get('session_user_member').then((res) => {
      this.id_member = res[0].id_member;
      this.fullname = res[0].full_name.toUpperCase();
      this.phone = res[0].phone;
      this.birthday = res[0].birthday;
      this.email = res[0].email;
      this.nik = res[0].nik;

      this.qrcodeId = this.id_member;

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DigitalcardPage');
  }

  saveDigitalCard() {
    var container = document.getElementById('cardmember');
    html2canvas(container).then(function (canvas) {
      var img = document.createElement('a');
      img.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
      img.download = 'digitalcard.jpg';
      img.click();
    });

    // var container = document.getElementById("cardmember");; // full page 
    // html2canvas(container, { allowTaint: true }).then(function (canvas) {

    //   var link = document.createElement("a");
    //   document.body.appendChild(link);
    //   link.download = "html_image.jpg";
    //   link.href = canvas.toDataURL();
    //   link.target = '_blank';
    //   link.click();
    // });

    // html2canvas(document.querySelector("#capture")).then(canvas => {
    //   document.body.appendChild(canvas)
    // });

    // html2canvas(container).then(function (canvas) {
    //   console.log(canvas)
    //   // var img = document.createElement('a');
    //   // img.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
    //   // img.download = 'digitalcard.jpg';
    //   // img.click();
    // });

    // var container = document.getElementById('cardmember')
    // html2canvas(container)
  }

  proccessQr() {

    this.storage.get('session_user_member').then((res) => {

      this.qrcodeId = this.id_member;

      const qrcode = QRCode;
      const self = this;
      qrcode.toDataURL(self.qrcodeId, { errorCorrectionLevel: 'H' }, function (err, url) {
        self.qrcodeId = url;
      })
      document.getElementById('qrShow').style.display = 'none';
      document.getElementById('qrHide').style.display = '';
      document.getElementById('qr').style.display = '';

    });


  }

  hideQR() {
    document.getElementById('qrShow').style.display = '';
    document.getElementById('qrHide').style.display = 'none';
    document.getElementById('qr').style.display = 'none';
  }


}
