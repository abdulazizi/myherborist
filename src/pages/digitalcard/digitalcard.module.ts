import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DigitalcardPage } from './digitalcard';

@NgModule({
  declarations: [
    DigitalcardPage,
  ],
  imports: [
    IonicPageModule.forChild(DigitalcardPage),
  ],
})
export class DigitalcardPageModule {}
