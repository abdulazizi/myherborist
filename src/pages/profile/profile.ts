import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/Storage';
import { TransactionPage } from '../transaction/transaction';
import { EditprofilePage } from '../editprofile/editprofile';
import { DigitalcardPage } from '../digitalcard/digitalcard';
import { PostProvider } from '../../providers/post-provider';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  id_member: any;
  fullname: any;
  point: any;
  phone: any;
  nik: any;
  birthday: any;
  email: any;
  loader: any;

  constructor(
    public storage: Storage,

    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,

    public navCtrl: NavController,
    public navParams: NavParams) {

    this.storage.get('session_user_member').then((res) => {
      this.id_member = res[0].id_member;
      this.phone = res[0].phone;
      this.fullname = res[0].full_name.toUpperCase();
      this.getPoint(this.id_member, this.phone)
    });


  }

  ionViewDidEnter() {

    // this.point = '0';

    this.storage.get('session_user_member').then((res) => {
      console.log(res)
      this.phone = res[0].phone;
      this.birthday = res[0].birthday;
      this.email = res[0].email;
      this.nik = res[0].nik;
    });

  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  getPoint(id_member, phone) {
    let body = {
      phone: phone,
      id_member: id_member,
      aksi: 'get_point'
    };

    console.log(body);

    this.presentLoading('Mohon tunggu..');

    this.postPvdr.postData(body, 'Transaction').subscribe((data) => {

      if (data.success) {
        this.loader.dismiss();
        // code here

        this.point = data.result[0].balance;


      } else {

        this.loader.dismiss();
        // code here

        this.point = '0';

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getPoint(this.id_member, this.phone)

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  goTransaction() {
    this.navCtrl.push(TransactionPage)
  }

  goDigitalCard() {
    this.navCtrl.push(DigitalcardPage)
  }

  editData() {
    this.navCtrl.push(EditprofilePage, { phone: this.phone, nik: this.nik, birthday: this.birthday, email: this.email })
  }

}
