import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/Storage';
import { PostProvider } from '../../providers/post-provider';


@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {

  // newDate = new Date();
  // dateNow: String = new Date().toISOString();
  // time: any;

  edit_date: any;

  phone_validation: any;
  password_validation: any;
  phone: any;
  nik: any;
  birthday: any;
  email: any;

  loader: any;


  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {



    // const alert = this.alertCtrl.create({
    //   mode: 'ios',
    //   title: 'Date and Time',
    //   subTitle: 'Date Now : ' + this.dateNow + '<br>' + 'Time : ' + this.time,
    //   buttons: [
    //     {
    //       text: 'Ok',
    //       handler: () => {
    //         // code here
    //       }
    //     }
    //   ]
    // });
    // alert.present();

    this.getCurrentData(
      navParams.get('phone'),
      navParams.get('nik'),
      navParams.get('birthday'),
      navParams.get('email')
    );

  }

  getCurrentData(phone, nik, birthday, email) {
    this.phone = phone;
    this.nik = nik;
    this.birthday = birthday;
    if (this.birthday == '' || this.birthday == null) {

      var newDate = new Date();
      var dateNow1: String = new Date().toISOString();

      var dateTime: String = new Date(newDate.getTime() - newDate.getTimezoneOffset() * 60000).toISOString();
      var dateNow = dateNow1.substring(0, 10);
      var time = dateTime.substring(11, 19);

      this.edit_date = dateNow + ' ' + time;
      console.log(dateNow + ' ' + time)

      this.birthday = dateNow

    } else {
      this.birthday = birthday
    }
    this.email = email;

    console.log(this.phone)
    console.log(this.nik)
    console.log(this.birthday)
    console.log(this.email)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  underDev() {
    const toast = this.toastCtrl.create({
      message: 'Under Development',
      duration: 3000
    });
    toast.present();
  }

  chekcData() {
    let body = {
      phone: this.phone_validation,
      password: this.password_validation,
      aksi: 'login'
    };

    this.presentLoading('Mohon tunggu..');

    // this.storage.clear();

    this.postPvdr.postData(body, 'Login').subscribe((data) => {
      console.log(data.result)
      // var msg = data.msg;
      if (data.success) {
        // this.loader.dismiss();
        // code here
        this.updateData()

      } else {
        this.loader.dismiss();
        // code here
        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Info',
          subTitle: 'Nomor hanphone dan password tidak cocok. Silahkan coba lagi.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                // code here
              }
            }
          ]
        });
        alert.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();
    });
  }

  updateData() {

    let body = {
      nik: this.nik,
      birthday: this.birthday,
      email: this.email,
      edit_date: this.edit_date,
      phone: this.phone_validation,
      password: this.password_validation,
      aksi: 'update'
    };

    console.log(body)

    // this.presentLoading('Mohon tunggu..');

    // this.storage.clear();

    this.postPvdr.postData(body, 'Update').subscribe((data) => {
      console.log(data.pesan)
      // var msg = data.msg;
      if (data.success) {
        // this.loader.dismiss();
        // code here
        // this.storage.set('session_user_member', data.result);
        // this.navCtrl.pop()
        this.selectData()

      } else {
        this.loader.dismiss();
        // code here
        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Info',
          subTitle: 'Gagal update profile.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                // code here
              }
            }
          ]
        });
        alert.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();
    });
  }

  selectData() {
    let body = {
      phone: this.phone_validation,
      password: this.password_validation,
      aksi: 'login'
    };

    // this.presentLoading('Mohon tunggu..');

    // this.storage.clear();

    this.postPvdr.postData(body, 'Login').subscribe((data) => {
      console.log(data.result)
      // var msg = data.msg;
      if (data.success) {
        this.loader.dismiss();
        // code here
        this.storage.set('session_user_member', data.result);
        this.navCtrl.pop()

        const toast = this.toastCtrl.create({
          message: 'Berhasil update profil',
          duration: 3000
        });
        toast.present();

      } else {
        this.loader.dismiss();
        // code here
        const alert = this.alertCtrl.create({
          mode: 'ios',
          title: 'Info',
          subTitle: 'Gagal update profil.',
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                // code here
              }
            }
          ]
        });
        alert.present();

      }
    }, error => {

      this.loader.dismiss();
      const alert = this.alertCtrl.create
        ({
          mode: 'ios',
          title: 'Pehatian',
          subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
          buttons: ['OK']
        });
      alert.present();
    });
  }

}
