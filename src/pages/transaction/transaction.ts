import { Component } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/Storage';
import { TransdetailPage } from '../transdetail/transdetail';


@IonicPage()
@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})
export class TransactionPage {

  loader: any;
  phone: any;
  id_member: any;
  trans_list: any;

  constructor(
    public storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public postPvdr: PostProvider,
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.getTrans()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionPage');
  }

  async presentLoading(x) {
    this.loader = await this.loadingCtrl.create({
      content: x,
    });
    return await this.loader.present();
  }

  getTrans() {
    this.storage.get('session_user_member').then((res) => {

      this.id_member = res[0].id_member;
      this.phone = res[0].phone;

      let body = {
        phone: this.phone,
        id_member: this.id_member,
        aksi: 'get_trans'
      };

      console.log(body);

      this.presentLoading('Mohon tunggu..');

      this.postPvdr.postData(body, 'Transaction').subscribe((data) => {

        if (data.success) {
          this.loader.dismiss();
          // code here

          this.trans_list = [];
          var tgl = "";
          var tgl_1 = "";
          var tgl_2 = "";
          for (let i = 0; i < data.result.length; i++) {
            tgl_1 = data.result[i].just_date;
            if (tgl_1 != tgl_2) {
              tgl = tgl_1;
            } else {
              tgl = "";
            }
            this.trans_list.push({
              'add_date': data.result[i].add_date,
              'color': data.result[i].color,
              'counter_id': data.result[i].counter_id,
              'counter_name': data.result[i].counter_name,
              'id': data.result[i].id,
              'id_member': data.result[i].id_card,
              'just_date': tgl,
              'date': data.result[i].just_date,
              'name_location': data.result[i].name_location,
              'phone': data.result[i].phone,
              'point': data.result[i].point,
              'ref_number': data.result[i].ref_number,
              'tipe': data.result[i].tipe,
              'type': data.result[i].type
            })
            tgl_2 = data.result[i].just_date;
          }

          console.log(this.trans_list)

        } else {

          this.loader.dismiss();
          // code here

          document.getElementById('empty').style.display = '';

        }
      }, error => {

        this.loader.dismiss();
        const alert = this.alertCtrl.create
          ({
            mode: 'ios',
            title: 'Pehatian',
            subTitle: 'Tidak tersambung dengan internet, silahkan coba beberapa saat lagi.',
            buttons: ['OK']
          });
        alert.present();

      });

    });
  }

  getDetailTrans(
    add_date,
    color,
    counter_id,
    counter_name,
    id,
    id_member,
    date,
    name_location,
    phone,
    point,
    ref_number,
    tipe,
    type) {

    // const modal = this.modalCtrl.create(TransdetailPage, {
    //   add_date: add_date,
    //   color: color,
    //   counter_id: counter_id,
    //   counter_name: counter_name,
    //   id: id,
    //   id_member: id_member,
    //   date: date,
    //   name_location: name_location,
    //   phone: phone,
    //   point: point,
    //   ref_number: ref_number,
    //   tipe: tipe,
    //   type: type
    // });
    // modal.present();

    this.navCtrl.push(TransdetailPage, {
      add_date: add_date,
      color: color,
      counter_id: counter_id,
      counter_name: counter_name,
      id: id,
      id_member: id_member,
      date: date,
      name_location: name_location,
      phone: phone,
      point: point,
      ref_number: ref_number,
      tipe: tipe,
      type: type
    })
  }

}
