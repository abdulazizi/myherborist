import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-transdetail',
  templateUrl: 'transdetail.html',
})
export class TransdetailPage {

  add_date: any;
  color: any;
  counter_id: any;
  counter_name: any;
  id: any;
  id_member: any;
  date: any;
  name_location: any;
  phone: any;
  point: any;
  ref_number: any;
  tipe: any;
  type: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {

    this.getCurrentData(
      navParams.get('add_date'),
      navParams.get('color'),
      navParams.get('counter_id'),
      navParams.get('counter_name'),
      navParams.get('id'),
      navParams.get('id_member'),
      navParams.get('date'),
      navParams.get('name_location'),
      navParams.get('phone'),
      navParams.get('point'),
      navParams.get('ref_number'),
      navParams.get('tipe'),
      navParams.get('type')
    );

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransdetailPage');
  }

  getCurrentData(add_date, color, counter_id, counter_name, id, id_member, date, name_location, phone, point, ref_number, tipe, type) {
    this.add_date = add_date;
    this.color = color;
    this.counter_id = counter_id;
    this.counter_name = counter_name;
    this.id = id;
    this.id_member = id_member;
    this.date = date;
    this.name_location = name_location;
    this.phone = phone;
    this.point = point;
    this.ref_number = ref_number;
    this.tipe = tipe;
    this.type = type;

    // console.log(this.add_date)
    // console.log(this.color)
    // console.log(this.counter_id)
    // console.log(this.counter_name)
    // console.log(this.id)
    // console.log(this.id_member)
    // console.log(this.date)
    // console.log(this.name_location)
    // console.log(this.phone)
    // console.log(this.point)
    // console.log(this.ref_number)
    // console.log(this.tipe)
    // console.log(this.type)
  }

}
