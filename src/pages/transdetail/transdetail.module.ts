import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransdetailPage } from './transdetail';

@NgModule({
  declarations: [
    TransdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(TransdetailPage),
  ],
})
export class TransdetailPageModule {}
