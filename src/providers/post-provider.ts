import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostProvider {

	// put server address here
	server: string = "http://app.vci.co.id:88/vci_mobile_api/index.php/mobile_member/"


	constructor(public http: Http) {

	}

	postData(body, file) {

		let type = "application/x-www-form-urlencoded";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });
		return this.http.post(this.server + file, JSON.stringify(body), options).map(res => res.json());

	}

}