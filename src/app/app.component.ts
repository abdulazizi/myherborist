import { Component } from '@angular/core';
// import { Platform } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';
// import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/Storage';

import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;

  constructor(
    // platform: Platform, 
    // statusBar: StatusBar, 
    // splashScreen: SplashScreen, 
    private storage: Storage) {
    // platform.ready().then(() => {
    //   // Okay, so the platform is ready and our plugins are available.
    //   // Here you can do any higher level native things you might need.
    //   statusBar.styleDefault();
    //   splashScreen.hide();
    // });

    this.storage.get('session_user_member').then((res) => {
      if (res == null) {
        this.rootPage = LoginPage;
      } else {
        this.rootPage = DashboardPage;
      }
    });

  }
}

