import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// plugin
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/Storage';
// import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

// provider
import { PostProvider } from '../providers/post-provider';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { MenusPage } from '../pages/dashboard/dashboard';
import { SignupPage } from '../pages/signup/signup';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { TransactionPage } from '../pages/transaction/transaction';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { DigitalcardPage } from '../pages/digitalcard/digitalcard';
import { TransdetailPage } from '../pages/transdetail/transdetail';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    HomePage,
    DashboardPage,
    MenusPage,
    ProfilePage,
    TransactionPage,
    TransdetailPage,
    EditprofilePage,
    DigitalcardPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    // NgxQRCodeModule,
    IonicStorageModule.forRoot(),
    // IonicModule.forRoot(MyApp),
    IonicModule.forRoot(MyApp, {
      // animated: false,
      rippleEffect: false,
      mode: 'md'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    HomePage,
    DashboardPage,
    MenusPage,
    ProfilePage,
    TransactionPage,
    TransdetailPage,
    EditprofilePage,
    DigitalcardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PostProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },

  ]
})
export class AppModule { }
